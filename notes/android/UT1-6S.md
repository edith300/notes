# mad ut 1 solved
## 1.What is Android?
Android is an open source mobile operating system developed by Google Android is based on Linux Kernel. It is designed for screen and mobile devices such as smartphones and tablets.

## 2.Enlist features of Android OS? 
1.  Android is an open-source operating system.which Helps to develop customized android versions for future updates.
2. Anyone can customize the Android Platform. 
3. There are a lot of mobile applications that can be chosen by the consumer.
4. It provides many interesting features like weather details, opening screen, live RSS feeds etc.
5. It provides messaging services(SMS and MMS), web browser, connectivity (GSM, CDMA, Bluetooth, Wi-Fi etc.), media, handset layout etc.
6. External storage number of devices including Micro SD can read Micro SD card formatted.
7.  Multiple Media  format support.H.263, H.264, MPEG-4 SP,AAC, HE-AAC, AAC 5.1, MP3, Ogg, WAV, JPEG, PNG, GIF, and BMP.

## 3. State Advantages & Disadvantages of Android OS?
| Advantages | Disadvantages |
| --- | --- |
| Faster web browser | Many applications contains malware |
| Multiple Media format supports | Needs an internet connection |
| Open source framework | Heating of smartphone |
| Supports 2D and 3D graphics | Advertising in certain apps and in OS too |
| Supports multiple languages | Wasteful battery |
| Video calling support | Slow response |

## 4.  Explain the term Android ecosystem in detail. 
The Android ecosystem is nothing but the relationship between Users, Developers/Programmers, and Hardware equipment makers, the Android ecosystem is nothing but the mutual dependence between Users, Developers, and equipment makers. They are independent of each other so one cannot exist without the other.
The main block of the android ecosystem is:
1. Android User
2. Developer
3. Equipment Maker

**1.Android User**
Users buy handsets and software Application): Android users are also more likely to prefer saving their cost and love the openness of the platform also they like to customize their device.
**2.Developer:**
Android Developers are professional software developers in designing applications as well as developing applications for Android.
**3.Equipment Maker:**
Tech companies that make equipment, mobiles, gadgets.

## 5.Draw & Explain the Architecture of Android.
![](https://lh3.googleusercontent.com/TQWV4v_2eJRaR-5q9j6fU-6xGYJyNYKVDOgpOxggpS9VugvIAd6HOUNeV2dhdoZMyUUBhb2G2I77Z19swz-YEVlKCU3RsKCa3m_9OVY774KiOOpNjIzfXQkT97NvLggbcAyIF3wsGzZe5r-27Q)
## 6. Explain Android SDK? 
![](https://lh4.googleusercontent.com/Vwe_HZpymn96pG6nlYMKILDxdBeDvmS1QtOuNKAQ18zt_JTBPCcBhPNba-MhhT0-ypN7xUe-ZmX8qt3ouoNh_o2N708SKhoHT_Hgn5M4wQqgISx2GRGCq2T13s1aAQw1jHmT17GVGmfhplDwcQ)
-   The Android software development kit (sdk)  allows developers to create applications for the Android platform. The Android SDK is the most important software of Android which is installed.    
-   The Android SDK provides to test application API libraries and emulator documentation sample code developer tools and tutorials which helps us to build and test the debug apps from Android.
-   Android SDK provides the tools and application programming interface (API)  for developers to create applications on the Android platform using Java programming language.

## 7. Explain Android Virtual Device (AVD)? 
-   An Android virtual device (AVD)  represents a device configuration and AVD  is a computation that to find the characteristics of an Android phone tablet VR OS Android TV or Android OS device we want to simulate in the Android Emulator. 
-   An AVD  is an emulator configuration that enables the model actual device by calling hardware  and software options to emulate the Android Emulator.
-   An AVD is used for testing the android application.

### 8.What is an Emulator & Explain use of it? 
In computing, an emulator is software or hardware that allows one computer system (host) to function like another computer system. 
### Use of android emulator
1.  Android emulator allows the host machine to run software or use guest system-designed peripheral devices.
2.  Android Emulator is responsible for running debugging and testing Android applications.

## 9.  Explain Dalvik Virtual Machine (DVM) with a diagram? 
- Delicate register based virtual machine (VM)  that's been optimized to ensure that a device can run multiple instances efficiently.
- The DVM uses the device underlying Linux Kernel to handle low level functionality including security threading and process and memory management.
![](https://lh4.googleusercontent.com/_4VmiV7b2XkI-NbSO-vipeU79K1OmjP4y_Euos7w1Oi0_Z-PLdIFlBXnxSNxjomGxzxHdNf3z8O-S7PbSZkSqxPJm-gejuTs6BCfdv1zdSt1ytcqL6wNBSkLxh7tGM_fcKHfl7qxuDFpXwLVow)

## 10. Difference between JVM & DVM ? 
![](https://lh3.googleusercontent.com/PKZYo1FtVTIspDtgIjt_xxcr4r7HZ7AP3fFs5FucTFp8beH8UI1WGB_AVBQZFi2lQEvTfMnCSY3EzU6xfYS-rk9mHNN5D-9QHcOAfQRCoDEVSb4yXN1LUf_JcI1m9-dJcDv65o2S0gZJ-LCNaw)

## 11. Draw & Explain Directory Structure of Android Studio
![](https://lh6.googleusercontent.com/cdRlp7v53cp93ewfkzIxdAAMSGkS881tx-F-MzbA2kwq7BPT-oahkN_qT-n5j-ILSd9CW7S5qeNhkydZtDXgSP-LYsz1sRWR45pusqrA9cIl7KaPSgLQsus1_HSgIkv8-Qq4VNJy3TBruYaOeg)

When we create an application on Android studio with a file that the project is divided into 2 folders 
1)app 2) Gradle script . The add folder contains 3) subfolders (manifest Java ,res) They make over the application. They are divided so that it should be easy to determine which resource should go in which folder.

**1.Manifest Folder**
It contains an Android manifest.xml file that is generated by Android studio when we create a project. This file contains the configuration parameters of a project such as Permission services and additional libraries.
```xml
<?xml version="1.0"   ?>  
<manifest xmlns:android="">  
</manifest>
```
**2.Java Folder**
The main activity. Java file is automatically kept in this folder by Android studio all the classes will be available here and Android studio will even bundle together the package so that we can work with the file without having to go through all the folders.

**3.Res Folder**
This folder contains the file that helps us to separate the resource over application resource means all the needed files except the source code for example- for developing an app we need to include resource files such as app logo, photos, videos or animations.

**1.Drawable folder:**
A drawable folder contains graphics that can be drawn on screen for example image files.

**2.Layout Folder:**
Layout folder content XML file that can be used for layout this file are used to set up the layout for the activity and it is used for basic alignment of layout ,components , Widget etc. the activity_main .XML file is automatically created in this folder by Android studio.

**3.BitMap Folder:**
This folder contains the launcher icon for the app, launcher icon is a graphics that represents an app to the user.

**4.Value Folder:**
The value folder contains XML files that contain simple values such as string integers and colors. The value Folder is used to keep track of the value we will be using in our application.
  
**4.Gradle Script**
The apk files are built using the gradle build system which is integrated in the Android studio. When we start the app , the gradle build folder is automatically created. If we have any specific requirement we can specify in this folder.

## 12.  What are Layout & List types of Layouts?
A layout defines the structure for a user interface in your app, such as in an activity. All elements in the layout are built using a hierarchy of View and ViewGroup objects.
• Linear Layout
• Absolute Layout
• Frame Layout
• Relative layout
• Table Layout

![](https://lh5.googleusercontent.com/u8J2RUzfj-R32NlQUwRVSkkk5cXXFZVTXsUD8q16auw2D7R37DdiTKXElSl79BA7HfvN2GV5T0fopeR66Gmd4u-AdiDhA9nadB7sV-ZQ2IcPP-G-XkBH0koFBqQ77Tfdi_fQceFR84mOMdTAaQ)  


## 13.  What is LinearLayout & Explain with an example (.xml file).
Linear layout is a simple layout used in android for layout designing. In the Linear layout all the elements are displayed in linear fashion means all the childs/elements of a linear layout are displayed according to its orientation. The value for orientation property can be either horizontal or vertical.

```xml
<LinearLayout 

  xmlns:android="http://schemas.android.com/apk/res/android"   
  android:layout_width="fill_parent"
  android:layout_height="wrap_content"
  android:orientation="vertical">  
  <Button  
    android:layout_width="wrap_content"

    android:layout_height="wrap_content"

    android:text="Button1"

    android:id="@+id/button"/>  
</LinearLayout>
```
## 14.  Explain TableLayout with an example (.xml file).
In Android, Table Layout is used to arrange the group of views into rows and columns. Table Layout containers do not display a border line for their columns, rows or cells. A Table will have as many columns as the row with the most cells.
![](https://lh5.googleusercontent.com/zYyYbpRvxIbnOpRtovYV9OU9h31pj-iSNief7aoxvGFRoAPGl0pd7lWsCt0r_67V9bzm5mO2xr-15BKk3l9dFj1fmosK4aY8xL9kgbqs5l2ZM-vWs3WKgX7wXmSLpcou1jNTKqRpJUtpoIFoPA)
A table can also leave the cells empty but cells can’t span the columns as they can in HTML(Hypertext markup language).

```xml
<?xml version="1.0" encoding="utf-8"?>
<TableLayout
  xmlns:android="http://schemas.android.com/apk/res/android"
  android:layout_width="match_parent"
  android:layout_height="match_parent">
  <TableRow android:id="@+id/firstRow"
    android:layout_width="fill_parent"
    android:layout_height="wrap_content">
    <TextView
      android:layout_width="wrap_content"
      android:layout_height="wrap_content"
      android:text="Column Example"
      android:textSize="18dp"/>
  </TableRow>
</TableLayout>
```

## 15.  Write a program to place Name, Age & Mobile number centrally on the display screen using Absolute Layout .

```xml
<?xml version="1.0" encoding="utf-8"?>
<AbsoluteLayout
  xmlns:android="http://schemas.android.com/apk/res/android"
  xmlns:app="http://schemas.android.com/apk/res-auto"
  xmlns:tools="http://schemas.android.com/tools"
  android:layout_width="match_parent" 
  android:layout_height="match_parent"
  tools:context=".MainActivity">
  <TextView
    android:id="@+id/textView" 
    android:layout_width="wrap_content"
    android:layout_height="wrap_content" 
    android:layout_x="150dp"
    android:layout_y="250dp" 
    android:text="Name:" 
    android:textSize="24sp"/>

  <TextView
    android:id="@+id/textView2"
    android:layout_width="wrap_content" 
    android:layout_height="wrap_content" 
    android:layout_x="150dp" 
    android:layout_y="297dp" 
    android:text="Age:" 
    android:textSize="24sp"/>

  <TextView
    android:id="@+id/textView3"
    android:layout_width="wrap_content" 
    android:layout_height="wrap_content" 
    android:layout_x="150dp"
    android:layout_y="343dp" 
    android:text="Mobile no:"
    android:textSize="24sp"/>
</AbsoluteLayout
```

