## 1. Write a PHP program using expressions and operator (ternary operator, arithmetic operators and comparison operators)
```php
<?php
  $x = 10;
  $y = 6;
  echo("<br>The value of x=".$x);
  echo("<br>The value of y=".$y);
  echo("<br>Addition is ".($x + $y));
?>
```
O/P
```
The value of x=10
The value of y=6
Addition is 16
```

## 2. Write a PHP program to the use of decision making and control structures using:

### `if` statement
```php
<?php
$x = -12;

if ($x > 0) {
    echo "The number is positive";
}

else{
    echo "The number is negative";
}
?>
```
O/P
```
The number is negative
```

### `if else` statement
```php
<?php
$x = "August";

if ($x == "January") {
    echo "Happy Republic Day";
} elseif ($x == "August") {
    echo "Happy Independence Day!!!";
} else {
    echo "Nothing to show";
}
?>
```
O/P
```
Happy Independence Day!!!
```

### `switch case` statement
```php
<?php
$n = "Monday";

switch($n) {
    case "Monday":
        echo "Its Monday";
        break;
    case "Tuesday":
        echo "Its Tuesday";
        break;
    case "Wednesday":
        echo "Its Wednesday";
        break;
    case "Thuesday":
        echo "Its Thuesday";
        break;
    case "Friday":
        echo "Its Friday";
        break;
    case "Saturday":
        echo "Its Saturday";
        break;
    case "Sunday":
        echo "Its Suday";
        break;
    default:
        echo "Doesn't exist";
}
?>
```
O/P
```
Its Monday
```

## 3. Write a PHP program to the use of looping structure using:

### `while` statement
```php
<?php
$num = 0;

while ($num <= 10) {
  echo $num;
  $num++;
}
?>
```
O/P
```
012345678910
```

### `do while` statement
```php
<?php
$x = 1;

do {
  echo "The number is: $x <br>";
  $x++;
} while ($x <= 5);
?>
```
O/P
```
The number is: 1
The number is: 2
The number is: 3
The number is: 4
The number is: 5
```

## 4. Write a PHP program to the use of looping structure using for statement, for each statement.
```php
<?php
for ($x = 0; $x <= 5; $x++) {
  echo "The number is: $x <br>";
}
?>
```
O/P
```
The number is: 0
The number is: 1
The number is: 2
The number is: 3
The number is: 4
The number is: 5
```

## 5. Write a PHP program for creating and manipulating , associative array and multidimensional array.

### Indexed array
```php
<?php
$cars= array("Volvo", "BMW", "Toyota");
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";
?>
```
O/P
```
I like Volvo, BMW and Toyota.
```

### Associative arrays
```php
<?php
  $total = array("fyif"=>"58", "syif"=>"58", "tyif"=>"46");
  echo "Total student in TYIF:".$total['tyif'];
?>
```
O/P
```
Total student in TYIF:46
```

### Multidimensional array
```php
<?php
  $student = array(
    array("XYZ",3401,86.45),
    array("ABC",3402,87.42),
    array("PQR",3403,85.43),
  );
  echo "Student Name:".$student[0][0].": Roll No: ".$student[0][1].",
  Percentage: ".$student[0][2].".<br>";
  echo "Student Name:".$student[1][0].": Roll No: ".$student[1][1].",
  Percentage: ".$student[1][2].".<br>";
  echo "Student Name:".$student[2][0].": Roll No: ".$student[2][1].",
  Percentage: ".$student[2][2].".<br>";
?>
```
O/P
```
Student Name:XYZ: Roll No: 3401, Percentage: 86.45.
Student Name:ABC: Roll No: 3402, Percentage: 87.42.
Student Name:PQR: Roll No: 3403, Percentage: 85.43.
```

## 6. Write a PHP program to calculate

### Length of string
```php
<?php
  echo strlen("Hello world!");
?>
```
O/P
```
12
```

### Count the no of words in string
```php
<?php
  echo str_word_count("Hello world!");
?>
```
O/P
```
2
```

### Compare two string using string function.
```php
<?php
  echo strcmp("Hello world!", "Hello world");
?>
```
O/P
```
0
```

## 7. Write a PHP program using following string function:
- strrev()
- strpos()
- strrpos()
- str_replace()
- ucwords()
- strtoupper()
- strtolower()
```php
<?php
  echo strrev("Hello world!")."</br>";
  echo strpos("Hello world!", "world")."</br>";
  echo strrpos("I love php, I love php too!","php")."</br>";
  echo str_replace("world", "bvit", "Hello world!")."</br>";
  echo ucwords("Welcome to php world")."</br>";
  echo strtoupper("Information Technology")."</br>";
  echo strtolower("INFORMATION TECHNOLOGY")."</br>";
?>
```
O/P
```
!dlrow olleH
6
19
Hello bvit!
Welcome To Php World
INFORMATION TECHNOLOGY
information technology
```

## 8. Write a PHP program to use: and anonymous function.

### User define function
```php
<?php
  function writeMessage() {
    echo "Welcome to PHP world";
  }
  writeMessage();
?>
```
O/P
```
Welcome to PHP world
```

### Variable function
```php
<?php
  function writeMessage() {
    echo "Welcome to PHP world";
  }

  $w = "writeMessage";
  $w()
?>
```
O/P
```
Welcome to PHP world
```

### Anonymous function
```php
<?php
  $a=function() {
    echo "Anonymous function";
  };
  $a();
?>
```
O/P
```
Anonymous function
```

## 9. Write a PHP program to create PDF document by using graphics concept.
```php
<?php
  require('fpdf.php');
  $pdf = new FPDF();
  $pdf->AddPage();
  $pdf->SetFont('Arial','B',16);
  $pdf->Cell(40,10,'Hello World!');
  $pdf->Output();
?>
```
O/P
```
Hello World
```

## 10. Write a PHP program
### a) To inherit member of superclass in subclass
```php
<?php
class Shape {
  public $length;
  public $width;

  public function __construct($length, $width) {
    $this->length = $length;
    $this->width = $width;
  }
}

class Rect extends Shape {
  public $height;

  public function __construct($length,$width,$height) {
    $this->length = $length;
    $this->width = $width;
    $this->height = $height;
  }

  public function intro() {
    echo "This length is {$this->length}, the width is {$this->width}, and the
    height is {$this->height}";
  }
}

$r=new Rect(10,20,30);
$r->intro();
?>
```
O/P
```
This length is 10, the width is 20, and the height is 30
```

### b) Create constructor to initialize object of class by using object oriented concept.
```php
<?php
class emp {
  private $fname;
  private $lname;

  public function __construct($fname,$lname) {
    $this->fname=$fname;
    $this->lname=$lname;
  }
  public function showName() {
    echo "My name is ".$this->fname." ".$this->lname;
  }
}

$sid=new emp("Aman","Varma");
$sid->showName();
?>
```
O/P
```
My name is Aman Varma
```

## 11. Write a PHP program on

### Introspection
```php
<?php
  class Rectangle {
    var $dim1 = 2;
    var $dim2 = 10;

    function Rectangle ($dim1,$dim2) {
      $this->dim1 = $dim1;
      $this->dim2 = $dim2;
    }

    function area() {
    	return $this->dim1*$this->dim2;
    }
    function display ()
    {
     // any code to display info
    }
  }

  $S = new Rectangle(4,2);

  $class_properties = get_class_vars("Rectangle");
  $object_properties = get_object_vars ($S);
  $class_methods = get_class_methods("Rectangle");
  $object_class = get_class ($S);

  print_r($class_properties);
  print_r ($object_properties);
  print_r ($class_methods);
  print_r($object_class);
?>
```
O/P
```
Array ( [dim1] => 2 [dim2] => 10 ) Array ( [dim1] => 4 [dim2] => 2 ) Array ( [0] => Rectangle [1] => area [2] => display ) Rectangle
```

### Serialization.
```php
<?php
  echo "<h4>Serializing the array</h4></br>";
  $data = serialize(array("Red", "Green", "Blue"));
  echo $data ."<br>";

  echo " <h4>Unserialziing the array</h4></br>";
  print_r(unserialize($data));
?>
```
O/P
```
Serializing the array
a:3:{i:0;s:3:"Red";i:1;s:5:"Green";i:2;s:4:"Blue";}

Unserialziing the array
Array ( [0] => Red [1] => Green [2] => Blue )
```

## 12. Design a web page using following form controls:
- a) Textbox
- b) Radio button
- c) Check box
- d) Button
```php
<form action="welcome.php" method="get">
  Name: <input type="text" name="user">
  <br>

  <input type="radio" id="male" name="gender" value="male">Male
  <input type="radio" id="female" name="gender" value="female">Female
  <input type="radio" id="other" name="gender" value="other">Other
  <br>

  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">Bike
  <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">Car
  <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">Boat
  <br>

  <button name="delete">Delete</button>
  <input type="submit">
</form>
```

## 13. Design a web page using following form controls:
- a) List box
- b) Hidden field box
```php
<form action="<? $_PHP_SELF ?>" method="post">
  <select name="foods">
    <option value="Tomatoes">Tomatoes</option>
    <option value="Cucumbers">Cucumbers</option>
    <option value="Celery">Celery</option>
  </select>
  <br>
  <input type="submit" name="submit" value="Submit"/>
</form>

<?php
  $c= $_POST['foods'];
  if(isset($c)) {
    echo 'You have chosen '.$c;
  } else {
    echo "You haven't selected any foods";
  }
?>
```

## 14. Develop a web page with data validation.
```php
<form action="<?php $_PHP_SELF ?>" method="post">
  Name:<input type="text" name="name" required>
  <input type="submit">
</form>

<?php
  if (!preg_match("/^[a-zA-Z_ ]*$/", $_POST["name"])) {
    echo "Only letter and whitespace is allowed";
  }
?>
```

## 15. Write a PHP program to:

### Create cookies
```php
<?php
$cookie_name = "Teacher";
$cookie_value = "Aman";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
?>
<?php
if (!isset($_COOKIE[$cookie_name])) {
  echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
  echo "Cookie '" . $cookie_name . "' is set! <br>";
  echo "Value is: " . $_COOKIE[$cookie_name];
}
?>
```
O/P
```
Cookie 'Teacher' is set!
Value is: Aman
```

- Modify cookies value
Set cookie agien with setcookie function.

- Delete cookies
```php
<?php
$cookie_name = "Teacher";
$cookie_value = "Aman";
setcookie($cookie_name, "", time() - 3600, "/");
?>
<?php
if (!isset($_COOKIE[$cookie_name])) {
  echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
  echo "Cookie '" . $cookie_name . "' is set! <br>";
  echo "Value is: " . $_COOKIE[$cookie_name];
}
?>
```
O/P
```
Cookie named 'Teacher' is not set!
```

## 16. Write a PHP program to:
- Start session
- Get session variable
- Destroy session
```php
<?php
  session_start();

  $_SESSION["color"] = "Red";
  $_SESSION["animal"] = "Lion";

  echo $_SESSION["color"]." ".$_SESSION["animal"];

  session_unset();
  session_destroy();
?>
```
O/P
```
Red Lion
```

## 17. Write a PHP program for sending and receiving plain text message (sending email).
```php
<?php
  $to = "aav@altmail.com";
  $subject = "Importent message";
  $message = "Hello this is importent message";

  $status = mail($to, $subject, $message);

  if ($status == true) {
    echo "Mail is sent.";
  } else {
    echo "Mail can't be sent";
  }
?>
```
## 18. Write a PHP program to

### Create database
```php
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test"

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE DATABASE myDB";
if ($conn->query($sql) === TRUE) {
  echo "Database created successfully";
} else {
  echo "Error creating database: " . $conn->error;
}

$conn->close();
?>
```

### Creation of table.
```php
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "CREATE TABLE MyGuests(id INT(6))";

if ($conn->query($sql) === TRUE) {
  echo "Table MyGuests created successfully";
} else {
  echo "Error creating table: " . $conn->error;
}

$conn->close();
?>
```

## 19. Write a PHP program to Inserting and retrieving the query result operations and Update ,Delete operations on table data.
