## Write a php program to find the largest number among three numbers.

```php
<?php
  function largest($x, $y, $z) {
    $max = $x;

    if ($x >= $y) {
      if($x >= $z)
        $max = $x;
      else
        $max = $z;
    }
    else {
      if($y >= $z)
        $max = $y;
      else
        $max = $z;
    }

    echo "Largest number among $x, $y and $z is: $max\n";
  }

  largest(100, 50, 25);
?>
```

**Output:**
```
Largest Number Among 100,50,25 is: 100
```

## Write a php code for creating a basic image.

```php
<?php
  $img = imagecreate(500, 300);
  $bgcolor = imagecolorallocate ($img, 150, 200, 200);
  $fontcolor = imagecolorallocate($img, 120, 60, 180);
  imagestring($img, 12, 150, 120, "Demo Text1", $fontcolor);
  imagestring($img, 3, 150, 100, "Demo Text2", $fontcolor);
  header("Content-Type: image/png");
  imagepng($img);
?>
```

## Explain inheritance with examples.

In Inheritance  the new class can inherit the properties and methods from the
old class.

The old class is the base class also called a parent class and then the new
class is a derived  class also called a subclass.

**Example**:

```php
<?php
  class Person {
    var $name, $age;
  }
  class Employee extends Person {
    var $salary, $designation;
  }
?>
```

## Explain the concept of introspection in php with an example.

PHP Introspection is the ability of a program to examine an object's
characteristics such as its name, parent class (if any), properties and methods.

Introspection  in PHP offers a useful Ability  to examine classes' interfaces
properties methods. With introspection we can write  code that operates on any
object or class.

**Example**:

```php
<?php
  if (class_exists('MyClass')) {
    $myclass = new MyClass();
  }
?>
```

##  How to use overloading and overriding functions in php?

## Overloading

Method overloading is the ability to create a multiple function
of the same name with a different implementation  depending upon the types of
arguments.

How to use overloading in PHP?


```php
class MainClass {
  public function ShowTitle($parameter1) {
    echo "Best of the luck";
  }

  public function ShowTitle($parameter1, $parameter2) {
    echo "for exam";
  }
}

$object = new MainClass;
$object->ShowTitle('Hello');
```


## Overriding

In function overriding, both parent and child classes should have the same
function name with and number of arguments. It is used to replace the parent
method in child class.

```php
class ParentClass {
  function helloWorld() {
    echo "Parent";
  }
}
class ChildClass extends ParentClass {
  function helloWorld() {
    echo "\nChild";
  }
}

$p = new ParentClass;
$c = new ChildClass;
$p->helloWorld();
$c->helloWorld();
```

**Output:**

```
Parent
Child
```




## How to use the flip function in php.

The array_flip function flips/All keys with their associated values in array.

This function is very useful when we have a big/ large array.and we want to
know if a given value  is in the array.

**Syntax**:

```
array_flip(array_name)
```

**Example:**

```php
<?php
  $input = array("a" => 1, "b" => 1, "c" => 2);
  $flipped = array_flip($input);  //flip function is used
  print_r($flipped);
?>
```

**Output:**

```
Array([1] => b [2] => c)
```

## How to define properties and methods of a class? Explain with suitable examples.
1. Objects have access to special variables called. properties.
2. A property Can be valued as an array and even another object.
3. All properties go inside the curly braces.
4. The characteristics of a class are known as its properties.

**Example:**

```php
<?php
  class Student {
    var $roll_no;
    var $name;
    function display() {
      echo "Roll No: " . $this->roll_no. "<br>";
      echo "Name: " $this->name;
    }
  }

  $s1 = new Student;
  $s1->roll_no = 10;
  $s1->name = "Amar";
  $si->display();
?>
```

**Output:**

```
Roll No: 10
Name: Amar
```

A class method is exactly similar to PHP functions.

1.  Method is a function defined within the class.
2.  Methods on the other hand are functions that operate on the data.
3.  Method is also written within the braces {}.

The method can be accessed from outside the class.

**Example:**

```php
<?php
class Person {
	public $age = 25;

	public function printAge() {
		echo "The age is: $this->age";
	}
}

$person = new Person;
$person->age = 20;
$person->printAge();
?>
```

**Output:**

```
The age is: 20
```



## Explain the arithmetic operators that can be used in php with examples.

The PHP arithmetic operators are used with numeric values to perform common
arithmetical operations, such as addition, subtraction, multiplication etc.

**Example:**

```php
<?php
  $a = 42;
  $b = 20;

  $c = $a + $b;
  echo "Addition is: $c <br/>";
  $c = $a - $b;
  echo "Subtraction is: $c <br/>";
  $c = $a * $b;
  echo "Multiplication is: $c <br/>";
  $c = $a / $b;
  echo "Division is: $c <br/>";
  $c = $a % $b;
  echo "Modulus is: $c <br/>";
?>
```

**Output:**

```
Addition is: 62
Subtraction is: 22
Multiplication is: 840
Division is: 2.1
Modulus is: 2
```



## What is the use of anonymous functions in php? Explain with examples.

Anonymous functions, also known as closures, allow the creation of functions
which have no specified name. They are most useful as the value of callable
parameters, but they have many other uses.

```php
<?php
  $add = create_function('$a, $b', 'return ($a+$b); ');
  $r = $add(2,3);
  echo $r;
?>
```

**Outputs**:

```
5
```

## Explain cloning objects with examples.

Object cloning is the process in PHP to create a copy of an object. An object
copy is created by using the clone keyword when the object is cloned PHP will
perform Shallow copy  of all the object properties and properties that are
referenced to their variables will remain references.

**Example:**

```php
<?php
  // Program to create copy of an object
  // Creating class

  Class GFG {
    public$data1;
    public$data2;
    public$data3;
  }
  // creating object
  $obj = new GFG();

  // Creating clone or copy of object
  $copy= clone $obj;

  // Set values of $obj object
  $obj->datal = "PHP";
  $obj->data2 = "with";
  $obj->data3 = "Web";

  // Set values of copied object
  $copy->data1 = "Computer";
  $copy->data2 = "IT ";
  $copy->data3 = "Portal";

  // Print values of $obj object
  echo "$obj->data1 $obj->data2 $obj->data3\n";

  // Print values of $copy object
  echo "$copy->data1 $copy->data2 $copy->data3\n";
?>
```

**Output:**

```
PHP with Web
Computer IT Portal
```

## Explain indexed and associative arrays with examples.

Indexed array: An array having only integer keys is typically referred to as an
indexed array and index arrays can store numbers, drinks and any object but
their index will be represented by number.

**Example:**

```php
<?php
  // Define an indexed array
  $colors = array("Red", "Green", "Blue");
  // Printing array structure
  print_r($colors);
?>
```


**Output:**

```
Array ( [0] => Red [1] => Green [2] => Blue )
```

The associative arrays are very similar to numeric arrays in terms of
functionality but they are different in terms of their index. Associative arrays
will have their index as string so that you can establish a strong association
between key and values.

```php
<?php
  // Define an associative array
  $ages = array("Peter"=>22, "Clark"=>32, "John"=>28);
  // Printing array structure
  print_r($ages);
?>
```

**Output:**

```
Array ( [Peter] => 22 [Clark] => 32 [John] => 28 )
```

## How to define constructor and destructor in php with an example?

When we create a new object it is useful to initialize its properties. PHP
provides us with a special method  to help initialize an object's properties
called a constructor.

In PHP, a method of special name __construct acts as a constructor.

**Example:**

```php
<?php
  class myclass{
     function __construct(){       //constructor is used
        echo "object initialized";
     }
  }

  $obj=new myclass();
?>
```

**Output:**

```
object initialized
```

The destructor  method will be called as soon as there are no other references
to a particular object.

PHP destructors  allow us to clean up(destroy)  resources before PHP releases
the object from the memory.

**Example:**

```php
<?php
  class myclass{
    function __construct(){
      echo "object is initialized\n";
    }
    function __destruct(){				// destructor is used
      echo "object is destroyed\n";
    }
  }
  $obj=new myclass();
?>
```

**Output:**

```
object is initialized
object is destroyed
```

## Explain the following string function with an example.

1. strpos()
2. strlen()
3. strcmp()
4. str_word_count()
5. strpos()

### strpos()

Find the last occurrence of the substring in the main string.

**Syntax**:

```
 strpos(string,find,start)
```

**Example:**

```php
<?php
  echo strpos("I love php, I love php too!","php");
?>
```
**Output**:

```
7
```

### strlen()

It is used to find the length of a string or returns the length of a string including all white spaces and special characters.

**Syntax:**

```
strlen(string)
```

**Example:**

```php
<?php
  echo strlen("Hello");
?>
```

**Output:**

```
5
```

### strcmp()

This function compares two strings and tells whether a string is greater or less or equal to another string.

**Syntax:**

```
strcmp(string1,string2)
```

**Example:**

```php
<?php
  $str1 = "hello php";
  $str2 = "hello php";
  echo strcmp($str1, $str2). " because both strings are equal. ";
?>
```

**Output:**

```
0 because both strings are equal.
```

### str_word_count()

It is used to return information about words used in a string or counts the number of words in a string.

**Example:**

```php
<?php
  echo str_word_count("Hello world!");  //there are 2 words hello and world
?>
```

**Output:**

```
2
```

## Write a program using a foreach loop.

**Example:**

```php
<?php
  //declare array
  $season = array ("Summer", "Winter", "Autumn", "Rainy");

  //access array elements using foreach loop
  foreach ($season as $element) {
    echo "$element";
    echo "</br>";
  }
?>
```

**Output:**

```
Summer
Winter
Autumn
Rainy
```

## Explain serialization with suitable examples.

A string representation of any object in the form of byte-stream is obtained by
serialize() function in PHP. All property variables of the object are contained
in the string and methods are not saved. This string can be stored in any file.

**Example:**

```php
<?php
  $data = serialize(array("Red", "Green", "Blue"));
  echo $data;
?>
```

**Output:**

```
a:3:{i:0;s:3:"Red";i:1;s:5:"Green";i:2;s:4:"Blue";}
```

## Define `imagecolorallocate()` function along with suitable examples.

`imagecolorallocate()` function is another inbuilt PHP function mainly used to
implement a new color to an image. It returns the color of an image in an RGB
format (RED GREEN BLUE).

**Example:**

```php
<?php
  $im = imagecreate(100, 100);
  // sets background to red
  $background = imagecolorallocate($im, 255, 0, 0);
  // sets some colors
  $white = imagecolorallocate($im, 255, 255, 255);
  $black = imagecolorallocate($im, 0, 0, 0);
  // hexadecimal way
  $white = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
  $black = imagecolorallocate($im, 0x00, 0x00, 0x00);
?>
```

## Write a program to create a filled rectangle.

```php
<?php
  // Create an image of given size
  $image = imagecreatetruecolor(500, 300);
  $green = imagecolorallocate($image, 0, 153, 0);
  // Draw the rectangle of green color
  imagefilledrectangle($image, 20, 20, 480, 280, $green);
  // Output image in png format
  header("Content-type: image/png");
  imagepng($image);
  // Free memory
  imagedestroy($image);
?>
```

## Explain any three data types used in php.

1. **Integer:** integer data type used to specify a numeric value  without a fractional component the range of integers.
2. **Strings:** a string is a sequence of characters where characters are the same as a byte.
3. **Boolean:**   boolean  value can be either true or false both are  case-insensitive.

## Differentiate between implode and explode function.
| No.   | Implode                                                                                                                                                         | Explode                                                                                                                                                                     |
| ----- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1     | The implode() function returns a string from the elements of an array.                                                                                          | explode function breaks a string  into smaller parts and stores it  as an array.                                                                                            |
| 2     | The implode() function accepts its  parameters in either order.  However, for consistency with explode(),  you should use the documented order of  arguments.   | The explode() function splits  these strings based on a specific  delimiter and returns an array that  contains elements which are sustained  by the splitting operation.   |
| 3     | syntax :-string implode (pieces)                                                                                                                                | array explode (delimiter, string, limit)                                                                                                                                    |
| 4     | **Example**:  ```php <?php $arr = array("I", "love", "PHP."); $pole = implode(" ", $arr); \ echo "$pole"; ?> ```  Output: I love PHP.                           | Example: <?php  $string = "I love php."; $pole = explode(" ", $string); \ \ print_r($pole); ?>  Output: Array ( [0] => I [1] => love [2] => php. )                          |

## State the variable function. Explain it with an example.

PHP supports the concept of variable function means that we can call a function
based on a value of a variable if a variable name has a round parentheses
appended to it PHP will look for a function with the same name as a whatever
variable Evaluates to and will attempt to execute it.

**Example:**

```php
<?php
  function add($x, $y){
    echo $x + $y;
  }
  $var =  "add";
  $var(10,20);
?>
```

**Output:**

```
30
```
